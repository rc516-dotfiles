noremap <up> <nop>
inoremap <up> <nop>
noremap <down> <nop>
inoremap <down> <nop>
noremap <left> <nop>
inoremap <left> <nop>
noremap <right> <nop>
inoremap <right> <nop>

"set number
set hlsearch
if executable("par")
	set formatprg=par\ w72qregs0
endif
"set spell
"set spelllang=en_gb,pl
set hidden
"set list
"set listchars=tab:▸\ ,eol:¬
nmap <leader>l :set list!<CR>
highlight NonText guifg=#4a4a59
highlight SpecialKey guifg=#4a4a59

set background=dark

if has("autocmd")
filetype on
	autocmd BufNewFile,BufRead *.rss,*.atom setfiletype xml
	au BufRead,BufNewFile *.cf set ft=cf3
endif

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set smartindent
"set cindent
